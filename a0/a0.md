# Activity 0 : GitLab Setup for Assignments

Instructor : Homeyra Pourmohammadali

MTE 140 - Data Structures and Algorithms

Winter 2021 (Online)


University of Waterloo

Due: 11:00pm, Friday, January 29 2021

## Purpose of this activity

In this activity, you will practice setting up an online repository and version control system (GitLab at UWaterloo). It will be used for all future assignments. Version control systems (VCS) are widely used to manage programming projects. This activity is not graded and is only needed for working on your assignments.

## Instruction

### Step 1 - Fork git repo

#### Step 1.1
Sign in to GitLab at UW ```https://git.uwaterloo.ca/users/sign_in``` with your WatIAM credentials (without @uwaterloo.ca).

#### Step 1.2
Go to the public repository for MTE 140, at ```https://git.uwaterloo.ca/d24lau/mte140-w21```.

#### Step 1.3
Fork the project to your own account.

Click on the 'Fork' button at the top right of the screen.

Click the 'Select' button underneath your name.

You now have a copy of the MTE 140 repo in your git.uwaterloo.ca (GitLab) account.

### Step 2 - Setup JetBrains CLion

#### Step 2.1
Obtain a free JetBrains educational license.

Go to ```https://jetbrains.com/shop/eform/students```, and submit the form.

#### Step 2.2
Download and install JetBrains CLion.

Go to ```https://jetbrains.com/clion/download``` to download CLion.

#### Step 2.3
Start CLion for the first time.

Upon starting CLion for the first time, you will be prompted to enter your JetBrains account information. Use the userid and password that you established after obtaining your JetBrains educational license from Step 2.1.

#### Step 2.4
Connect CLion to the git repo.

On the CLion screen titled "Welcome to CLion", click the "Get from VCS" button.

At the left side of the screen, verify that "Repository URL" is selected. Then, verify that the "Version control:" drop-down is set to "Git".

Enter the URL of repo that you created in Step 1.3. It is generally of the form ```https://git.uwaterloo.ca/<userid>/mte140-w21```.

Click the 'Clone' button.

### Step 3 - First build
